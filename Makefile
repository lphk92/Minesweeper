SHELL=/bin/bash
.RECIPEPREFIX += 

BUILD_DIR = bin/

build: ;
    mkdir -p $(BUILD_DIR)
    javac -d $(BUILD_DIR) -sourcepath src/ src/Main.java

run: build
    java -classpath $(BUILD_DIR) Main

clean: ;
    rm -rf $(BUILD_DIR)
